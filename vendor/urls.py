from django.conf.urls import url,include
from .views import Railways,VendorViewset,PointViewset,FoodDetails,ClaimApi,FcmToken,FCMSender
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
urlpatterns = [
  url(r'^test-rail',Railways.as_view()),
  url(r'^login',obtain_jwt_token),
  url(r'^food-detail',FoodDetails.as_view()),
  url(r'^claim',ClaimApi.as_view()),
  url(r'^fcm',FcmToken.as_view()),
  url(r'^notify',FCMSender.as_view())

]


router = DefaultRouter()
router.register(r'vendor', VendorViewset)
router.register(r'point', PointViewset)

urlpatterns += router.urls
