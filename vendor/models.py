from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Vendor(User):

    class Meta:
        proxy = True


class Point(models.Model):

    owner =  models.ForeignKey(Vendor)
    value = models.IntegerField()


class Pantry(User):

    class Meta :
        proxy = True


class PatnryProfile(models.Model):

    vendor_id =  models.ForeignKey(Vendor,related_name='vendor')
    owner  = models.ForeignKey(Pantry,related_name='pantry',null=True,blank=True)
    contact =models.CharField(max_length=10)
    location =  models.CharField(max_length=100)


class NGO(User):

    class Meta :
        proxy = True

class NgoHelper(models.Model):

    owner =  models.ForeignKey(NGO)
    station_code = models.CharField(max_length=1000)
    fcm_token =  models.CharField(max_length=10000,null=True,blank=True)


class FoodDetail(models.Model):

    train = models.CharField(max_length=10000)
    veg = models.IntegerField()
    nonveg = models.IntegerField()
    claimed = models.BooleanField()
    ngo = models.ForeignKey(NGO,null=True,blank=True)
    station = models.CharField(max_length=1000,null=True,blank=True)