from django.contrib import admin
from .models import Vendor, Point,Pantry,NGO,NgoHelper,PatnryProfile
# Register your models here.
#
# @admin.register(PatnryProfile)
# class PantryProfileAdmin(admin.ModelAdmin):
#
#     pass
#
# @admin.register(NgoHelper)
# class NGOHelperAdmin(admin.ModelAdmin):
#
#     pass

admin.site.register(Vendor)
admin.site.register(Point)
admin.site.register(Pantry)
admin.site.register(NGO)
admin.site.register(NgoHelper)
admin.site.register(PatnryProfile)
# admin.site.register(PatnryProfile,PantryProfileAdmin)