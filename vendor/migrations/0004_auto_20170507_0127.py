# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-06 19:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vendor', '0003_fooddetail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fooddetail',
            name='ngo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='vendor.NGO'),
        ),
        migrations.AlterField(
            model_name='fooddetail',
            name='station',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]
