# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-06 20:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vendor', '0004_auto_20170507_0127'),
    ]

    operations = [
        migrations.CreateModel(
            name='NgoHelper',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('station_code', models.CharField(max_length=1000)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='vendor.NGO')),
            ],
        ),
        migrations.CreateModel(
            name='PatnryProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact', models.CharField(max_length=10)),
                ('location', models.CharField(max_length=100)),
                ('vendor_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='vendor.Vendor')),
            ],
        ),
    ]
