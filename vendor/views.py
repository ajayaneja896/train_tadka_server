from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
import  urllib3
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from .serializers import VendorSerializer,PointSerializer
from rest_framework.permissions import AllowAny
from django.db import  transaction
import  json
from django.utils import timezone,dateparse
from.models import Vendor,Point,FoodDetail,NgoHelper,NGO
from fcm_django.models import FCMDevice
import  datetime
import time
from dateutil import parser
# Create your views here.

class Railways(APIView):


    def get(self,request):
        http = urllib3.PoolManager()
        res = http.request('GET',"http://api.railwayapi.com/live/train/12011/doj/20170505/apikey/3vxnwgoe/")
        print(res.status)
        return Response(res.data)


class ClaimApi(APIView):

    def post(self,request):
        data_dict = request.data

        with transaction.atomic():
            food_id =  data_dict['food_id']
            ngo_id = data_dict['ngo']
            fc = FoodDetail.objects.get(id=food_id)
            if fc.claimed == False:
                fc.claimed =True
                fc.ngo =  ngo_id
                fc.station = data_dict['station']
                fc.save()


class VendorViewset(ModelViewSet):

    serializer_class =  VendorSerializer
    queryset =  Vendor.objects.all()


class PointViewset(ModelViewSet):

    serializer_class = PointSerializer
    queryset = Point.objects.all()

class FoodDetails(APIView):

    permission_classes = (AllowAny,)

    def parse_response(self,res_data,data_dict):

        stations =  res_data['route']
        to_arrive = []
        for station in stations :
            print(station['has_departed'], " ", station['station_'])
            date_string =  station['scharr_date']
            print(date_string)
            dt = parser.parse(date_string)
            hours = station['scharr'].split(':',1)
            if(len(hours)>1):
                dt = dt+datetime.timedelta(hours=int(hours[0]))
            if len(hours)>1:
                dt = dt+datetime.timedelta(minutes=int(hours[1]))
            difference = dt - datetime.datetime.now()
            # dt_final =  dt +  datetime.timedelta(hours=int(hours[0]),minutes=int(hours[1]))
            # print(dt_final)
            difference = difference.seconds
            print(difference)
            if station['has_departed'] == False and difference>=0 and difference<=10000:
                to_arrive.append(station['station_'])
        print(to_arrive)
        for st in to_arrive :
            print(st)
            ng_objects = NgoHelper.objects.filter(station_code = st['code'])
            print(ng_objects)
            for ng_obj in ng_objects:
                fc = FCMDevice.objects.create(device_id="123",
                                              registration_id=  ng_obj.fcm_token , type=2)
                fc.send_message(title="Food Alert", body="You can recieve food packets",data= {
                    'station' :  st['code'],
                    'veg' : data_dict['veg'],
                    'nonveg' : data_dict['nonveg'],
                    'train' : data_dict['train']
                })


    def railway_api_hit(self,data_dict):
        with transaction.atomic():
            detail =  FoodDetail.objects.create(train=data_dict['train'],veg=data_dict['veg'],nonveg=data_dict['nonveg'],claimed=False)
            detail.save()
            http = urllib3.PoolManager()
            train_number = data_dict['train']
            datetime = timezone.now()
            date_today  = datetime.strftime("%Y%m%d")
            url_hit = "http://api.railwayapi.com/live/train/"+train_number+"/doj/"+date_today+"/apikey/3vxnwgoe/"
            http = urllib3.PoolManager()
            res = http.request('GET',url_hit)
            res_data = json.loads(res.data.decode('utf-8'))
            print(res_data)
            print("\n here \n ")
            self.parse_response(res_data,data_dict)



    def post(self,request):
        data_dict  = request.data
        print(data_dict)
        self.railway_api_hit(data_dict)
        return Response({"saved":"true"},content_type='application/json')


class FcmToken(APIView):

    permission_classes = (AllowAny,)
    def post(self,request):

        data_dict =  request.data
        fcm_token = data_dict['fcm']
        print(fcm_token)
        ngo = NGO.objects.get(username=data_dict['username'])
        print(ngo)
        ngo_object = NgoHelper.objects.get(owner=ngo)
        ngo_object.fcm_token=fcm_token
        ngo_object.save()
        print(ngo_object.fcm)
        return  Response({"saved" : "True"},content_type='application/json')

    def get(self,request):
        owner = NGO.objects.get(username = 'test_ngo')
        ng = NgoHelper.objects.get(owner=owner)
        return Response(ng.fcm_token)


class FCMSender(APIView):

    def get(self,request):
        fc = FCMDevice.objects.create(device_id = "123",registration_id="fDAT7UP3DDY:APA91bFs6DtLMb2A1rLXbHZJkxjVhHqXbXNYT2cVmWAedn7_BI9jKnZT_YtbPpBQuHsZlvA18PC4TXH8yl98M4r0q2QQ-yL5h3c1RbVuXtXtc1h-_7NJMyni0_XJZsEC0R456wgZROw3"
                                      ,type=2)
        fc.send_message(title="Title", body="Message")